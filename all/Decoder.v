module Decoder(
    instr_op_i,
	RegWrite_o,
	ALU_op_o,
	ALUSrc_o,
	RegDst_o,
	Branch_o,
	sign_o
);

parameter R_TYPE_OP = 6'b000000;
parameter ADDI_OP = 6'b001000;
parameter BEQ_OP = 6'b000100;
parameter ORI_OP = 6'b001101;


input[5:0] instr_op_i;
output reg RegWrite_o;
output reg[1:0] ALU_op_o;
output reg ALUSrc_o;
output reg RegDst_o;
output reg Branch_o;
output reg sign_o;

always @(*)
begin
    case(instr_op_i)
        R_TYPE_OP:begin //R-type
            ALU_op_o = 2'b10;
            RegWrite_o = 1;
            ALUSrc_o = 0;
            RegDst_o = 1;
            Branch_o = 0;
            sign_o = 1;
        end
        ADDI_OP:begin //addi
            ALU_op_o = 2'b00;
            RegWrite_o = 1;
            ALUSrc_o = 1;
            RegDst_o = 0;
            Branch_o = 0;
            sign_o = 1;
        end
        BEQ_OP:begin //beq
            ALU_op_o = 2'b01;
            RegWrite_o = 0;
            ALUSrc_o = 0;
            RegDst_o = 1'bx;
            Branch_o = 1;
            sign_o = 1;
        end
        ORI_OP:begin //ori
            ALU_op_o = 2'b10;
            RegWrite_o = 0;
            ALUSrc_o = 0;
            RegDst_o = 0;
            Branch_o = 0;
            sign_o = 0;
        end
        default:begin
            ALU_op_o = 2'bx;
            RegWrite_o = 0;
            ALUSrc_o = 1'bx;
            RegDst_o = 1'bx;
            Branch_o = 0;
            sign_o = 1'bx;
        end
    endcase
end
endmodule


