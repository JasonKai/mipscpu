module ProgramCounter(
    clk_i,
    rst_n,
    pc_in_i,
    pc_out_o
);

input clk_i,rst_n;
input[31:0] pc_in_i;
output[31:0] pc_out_o;
reg[31:0] pc_out_o;

always @(posedge clk_i or negedge rst_n)begin
    if(!rst_n)
        pc_out_o <= 0;
    else
        pc_out_o <= pc_in_i;
end

endmodule
