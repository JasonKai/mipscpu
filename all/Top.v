module Simple_Single_CPU(
        clk_i,
		rst_n
);

input clk_i, rst_n;

wire[31:0] instr_addr;
wire[31:0] instr;
wire[4:0] write_reg_addr;
wire[31:0] extended_immediate;
wire[31:0] sl2_immediate;
wire[31:0] rs_data;
wire[31:0] rt_data;
wire[31:0] alu_input;
wire[31:0] alu_result;
wire iszero;
wire[3:0] alu_ctrl;
wire[31:0] pc_plus4;
wire[31:0] branch_target;
wire[31:0] nextpc;

wire RegDst;
wire[1:0] ALUOp;
wire ALUSrc;
wire branch;
wire RegWrite;
wire branchornot;
wire sign;

wire overflow;
wire cout;

assign branchornot = branch & iszero;


ProgramCounter PC(
    .clk_i(clk_i),
    .rst_n(rst_n),
    .pc_in_i(nextpc),
    .pc_out_o(instr_addr)
);

Instr_Mem IM(
    .pc_addr_i(instr_addr),
    .instr_o(instr)
);

Adder adder1(
    .src1_i(instr_addr),
	.src2_i(32'd4),
	.sum_o(pc_plus4)
);

MUX_2to1 # (.size(5)) mux_reg(
    .data0_i(instr[20:16]),
    .data1_i(instr[15:11]),
    .select_i(RegDst),
    .data_o(write_reg_addr)
);

Reg_File RF(
    .clk_i(clk_i),
	.rst_n(rst_n),
    .RSaddr_i(instr[25:21]),
    .RTaddr_i(instr[20:16]),
    .RDaddr_i(write_reg_addr),
    .RDdata_i(alu_result),
    .RegWrite_i(RegWrite),
    .RSdata_o(rs_data),
    .RTdata_o(rt_data)
);

Decoder decoder(
    .instr_op_i(instr[31:26]),
	.RegWrite_o(RegWrite),
	.ALU_op_o(ALUOp),
	.ALUSrc_o(ALUSrc),
	.RegDst_o(RegDst),
	.Branch_o(branch),
	.sign_o(sign)
);

Sign_Extend signextend(
    .data_i(instr[15:0]),
    .data_o(extended_immediate),
    .sign_i(sign)
);

 ALU_Ctrl aluctrl(
    .funct_i(instr[5:0]),
    .ALUOp_i(ALUOp),
    .ALUCtrl_o(alu_ctrl)
);

MUX_2to1 mux_alu(
    .data0_i(rt_data),
    .data1_i(extended_immediate),
    .select_i(ALUSrc),
    .data_o(alu_input)
);

Shift_Left_Two_32 shiftleft(
    .data_i(extended_immediate),
    .data_o(sl2_immediate)
);

alu alu1(
    .src1(rs_data),          // 32 bits source 1          (input)
    .src2(alu_input),          // 32 bits source 2          (input)
    .ALU_control(alu_ctrl),
    .shamt(instr[10:6]),   // 4 bits ALU control input  (input)
    .result(alu_result),        // 32 bits result            (output)
    .zero(iszero),          // 1 bit when the output is 0, zero must be set (output)
    .cout(cout),          // 1 bit carry out           (output)
    .overflow(overflow)       // 1 bit overflow            (output)
);

Adder adder2(
    .src1_i(sl2_immediate),
	.src2_i(pc_plus4),
	.sum_o(branch_target)
);

MUX_2to1 mux_branch(
    .data0_i(pc_plus4),
    .data1_i(branch_target),
    .select_i(branchornot),
    .data_o(nextpc)
);

endmodule
