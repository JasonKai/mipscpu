`timescale 1ns/1ps

module alu(
           src1,          // 32 bits source 1          (input)
           src2,          // 32 bits source 2          (input)
           ALU_control,   // 4 bits ALU control input  (input)
           shamt,
           result,        // 32 bits result            (output)
           zero,          // 1 bit when the output is 0, zero must be set (output)
           cout,          // 1 bit carry out           (output)
           overflow       // 1 bit overflow            (output)
           );

parameter AND_ctrl = 4'b0000;
parameter OR_ctrl = 4'b0001;
parameter ADD_ctrl = 4'b0010;
parameter SLL_ctrl = 4'b0011;
parameter SLLV_ctrl = 4'b0100;
parameter SUB_ctrl = 4'b0110;
parameter SLT_ctrl = 4'b0111;
parameter SRL_ctrl = 4'b1000;
parameter SRLV_ctrl = 4'b1001;
parameter NAND_ctrl = 4'b1100;
parameter NOR_ctrl = 4'b1101;

input   [31:0]  src1;
input   [31:0]  src2;
input   [4-1:0] ALU_control;
input   [4:0]   shamt;

output reg [31:0]  result;
output reg         zero;
output reg         cout;
output reg         overflow;

reg[31:0] tmp;

always @(src1 or src2 or ALU_control)
begin
    tmp = 32'bx;
    cout = 1'b0;
    overflow = 1'b0;

    case(ALU_control)
        AND_ctrl:begin //and
            result = src1&src2;
        end
        OR_ctrl:begin //or
            result = src1|src2;
        end
        ADD_ctrl:begin //add
            {cout,result} = {1'b0, {src1}}+{1'b0, {src2}};
            if(src1[31] ^ src2[31]==0)begin
                overflow = result[31] ^ src1[31];
            end
        end
        SUB_ctrl:begin //sub
            {cout,result} = {1'b0, {src1}}+{1'b0,{~src2}}+1;
            if(src1[31] ^ src2[31]==1)begin   //have a logical error
                overflow = ~(result[31] ^ src1[31]);
            end

        end
        SLT_ctrl:begin //slt
            tmp = src1-src2;
            result = {31'b0, {tmp[31]}};
        end
        NOR_ctrl:begin //nor
            result = ~(src1|src2);
        end
        NAND_ctrl:begin //nand
            result = ~(src1&src2);
        end
        SLL_ctrl:begin
            result = src2 << shamt;
        end
        SLLV_ctrl:begin
           result = src2 << src1;
        end
        SRL_ctrl:begin
           result = src2 >> shamt;
        end
        SRLV_ctrl:begin
           result = src2 >> src1;
        end

        default:
            result = 32'bx;
    endcase
    zero = !(|result);
end




endmodule
