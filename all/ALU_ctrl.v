module ALU_Ctrl(
    funct_i,
    ALUOp_i,
    ALUCtrl_o
);

input[5:0] funct_i;
input[1:0] ALUOp_i;
output reg[3:0] ALUCtrl_o;

always @(*)begin
    case(ALUOp_i)
        2'b00:begin
            ALUCtrl_o = 4'b0010;
        end
        2'b01:begin
            ALUCtrl_o = 4'b0110;
        end
        2'b10:begin
            case(funct_i)
                6'b100000:begin //add
                    ALUCtrl_o = 4'b0010;
                end
                6'b100010:begin //sub
                    ALUCtrl_o = 4'b0110;
                end
                6'b100100:begin //and
                    ALUCtrl_o = 4'b0000;
                end
                6'b100101:begin //or
                   ALUCtrl_o = 4'b0001;
                end
                6'b101010:begin //slt
                    ALUCtrl_o = 4'b0110;
                end
            endcase
        end
    endcase
end

endmodule
