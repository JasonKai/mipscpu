module Instr_Mem(
    pc_addr_i,
    instr_o
);

input[31:0] pc_addr_i;
output[31:0] instr_o;
reg [31:0] Instr_Mem [0:31];
integer i;

assign instr_o = Instr_Mem[pc_addr_i/4];

initial begin
    for(i=0;i<32;i=i+1)
        Instr_Mem[i]=32'b0;
end
endmodule
