module Sign_Extend(
    data_i,
    data_o,
    sign_i
);
input[15:0] data_i;
input sign_i;
output[31:0] data_o;

reg[31:0] data_o;

always @(*) begin
    if(!sign_i)
        data_o = {16'b0,data_i};
    else
        data_o = {{16{data_i[15]}},data_i};
end
endmodule
