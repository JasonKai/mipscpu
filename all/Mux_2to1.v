module MUX_2to1(
    data0_i,
    data1_i,
    select_i,
    data_o
);

parameter size = 32;

input[size-1:0] data0_i,data1_i;
input select_i;
output[size-1:0] data_o;

assign data_o = select_i?data1_i:data0_i;

/*if(select_i==1'b0)begin
    data_o = data0_i;
end
else begin
    data_o = data1_i;
end
*/
endmodule
