module Adder(
    src1_i,
	src2_i,
	sum_o
);

input [31:0] src1_i,src2_i;
output[31:0] sum_o;

assign sum_o = src1_i+src2_i;

endmodule
