module Reg_File(
    clk_i,
	rst_n,
    RSaddr_i,
    RTaddr_i,
    RDaddr_i,
    RDdata_i,
    RegWrite_i,
    RSdata_o,
    RTdata_o
);

input clk_i,rst_n;
input[4:0] RSaddr_i,RTaddr_i,RDaddr_i;
input[31:0] RDdata_i;
input RegWrite_i;
output[31:0] RSdata_o,RTdata_o;

//reg [31:0] RSdata_o,RTdata_o;
reg [31:0] Reg_File [0:31];

integer i;

assign RSdata_o = Reg_File[RSaddr_i];
assign RTdata_o = Reg_File[RTaddr_i];

always @(posedge clk_i or negedge rst_n)begin
    if(!rst_n)begin
        for(i=0;i<32;i=i+1)
            Reg_File[i] <= 31'b0;
    end
    else begin
        if(RegWrite_i)begin
            Reg_File[RDaddr_i] = RDdata_i;
        end
        else begin
            Reg_File[RDaddr_i] = Reg_File[RDaddr_i];
        end
    end
end

endmodule
